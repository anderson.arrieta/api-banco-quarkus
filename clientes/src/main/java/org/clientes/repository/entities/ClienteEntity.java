package org.clientes.repository.entities;

import jakarta.persistence.*;
import lombok.*;
import org.clientes.model.Persona;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "clientes")
@NamedQuery(name = "ClienteEntity.findAll", query = "SELECT c FROM ClienteEntity c")
public class ClienteEntity extends PersonaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String clave;
    private String estado;

    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", clave='" + clave + '\'' +
                ", estado='" + estado + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", genero='" + getGenero() + '\'' +
                ", edad=" + getEdad() +
                ", identificacion='" + getIdentificacion() + '\'' +
                ", direccion='" + getDireccion() + '\'' +
                ", telefono='" + getTelefono() + '\'' +
                '}';
    }

}
