package org.clientes.repository;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManagerFactory;
import org.clientes.mapper.Mapper;
import org.clientes.model.Cliente;
import org.clientes.repository.entities.ClienteEntity;
import org.hibernate.reactive.mutiny.Mutiny;
import java.util.stream.Collectors;

@ApplicationScoped
public class ClienteMutinyRepo {

    private final Mutiny.SessionFactory sessionFactory;

    public ClienteMutinyRepo(EntityManagerFactory entityManagerFactory) {
        this.sessionFactory = entityManagerFactory.unwrap(Mutiny.SessionFactory.class);
    }

    public Uni<Void> crearCliente(Cliente cliente) {
        return sessionFactory.withTransaction(((session, transaction) -> session.persist(Mapper.map(cliente, ClienteEntity.class))))
                .flatMap(v -> Uni.createFrom().voidItem());
    }

    public Multi<Cliente> listarClientes() {

        return sessionFactory.withSession(session -> session.createNamedQuery("ClienteEntity.findAll", ClienteEntity.class)
                .getResultList()).map(u -> u.stream().map(v -> Mapper.map(v, Cliente.class)).collect(Collectors.toList()))
                .onItem().transformToMulti(c -> Multi.createFrom().iterable(c));

    }

    public Uni<Cliente> buscarCliente(Integer id) {
        return sessionFactory.withSession(session -> session.find(ClienteEntity.class, id))
                .onItem().transform(c -> Mapper.map(c, Cliente.class));
    }

    public Uni<Void> eliminarCliente(Integer id) {
        return sessionFactory.withTransaction((session, transaction) ->
                session.find(ClienteEntity.class, id)
                        .onItem().invoke(entity -> session.remove(entity))
                        .onItem().transform(entity -> (Void) null)
                        );
    }

}
