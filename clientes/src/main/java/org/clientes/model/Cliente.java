package org.clientes.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cliente extends Persona{
    private Integer id;
    private String clave;
    private String estado;

    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", clave='" + clave + '\'' +
                ", estado='" + estado + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", genero='" + getGenero() + '\'' +
                ", edad=" + getEdad() +
                ", identificacion='" + getIdentificacion() + '\'' +
                ", direccion='" + getDireccion() + '\'' +
                ", telefono='" + getTelefono() + '\'' +
                '}';
    }
}
