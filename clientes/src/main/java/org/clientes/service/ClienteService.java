package org.clientes.service;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.clientes.model.Cliente;
import org.clientes.repository.ClienteMutinyRepo;

@ApplicationScoped
public class ClienteService {

    @Inject
    ClienteMutinyRepo repo;

    public Uni<Void> crearCliente(Cliente cliente) {
        return repo.crearCliente(cliente);
    }

    public Multi<Cliente> listarClientes() {
        return repo.listarClientes();
    }

    public Uni<Cliente> buscarCliente(Integer id) {
        return repo.buscarCliente(id);
    }

    public Uni<Void> eliminarCliente(Integer id) {
        return repo.eliminarCliente(id);
    }

}
