package org.clientes.controller;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.clientes.model.Cliente;
import org.clientes.service.ClienteService;

@Path("/clientes")
public class ClienteController {

    @Inject
    ClienteService service;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Void> crearCliente(Cliente cliente) {
        return service.crearCliente(cliente);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Multi<Cliente> listarClientes() {
        return service.listarClientes();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Cliente> buscarCliente(@PathParam("id") Integer id) {
        return service.buscarCliente(id);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Void> eliminarCliente(Integer id) {
        return service.eliminarCliente(id);
    }

}
