package org.movimientos.repository.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "movimientos")
@NamedQuery(name = "MovimientoEntity.findAll", query = "SELECT m FROM MovimientoEntity m")
@NamedQuery(name = "MovimientoEntity.findByFecha", query = "SELECT m FROM MovimientoEntity m WHERE m.idCuenta = :id AND m.fechaMovimiento BETWEEN :fechaInicio AND :fechaFin")
public class MovimientoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer idCuenta;
    private LocalDateTime fechaMovimiento;
    private Float saldoInicial;
    private String tipoMovimiento;
    private Float valor;
    private Float saldo;

}
