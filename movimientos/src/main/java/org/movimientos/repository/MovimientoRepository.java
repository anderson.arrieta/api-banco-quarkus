package org.movimientos.repository;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManagerFactory;
import org.hibernate.reactive.mutiny.Mutiny;
import org.movimientos.mapper.Mapper;
import org.movimientos.model.Movimiento;
import org.movimientos.repository.entities.MovimientoEntity;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

@ApplicationScoped
public class MovimientoRepository {

    private final Mutiny.SessionFactory sessionFactory;

    public MovimientoRepository(EntityManagerFactory entityManagerFactory) {
        this.sessionFactory = entityManagerFactory.unwrap(Mutiny.SessionFactory.class);
    }

    public Uni<Void> guardarMovimiento(Movimiento movimiento) {
        return sessionFactory.withTransaction(((session, transaction) -> session.persist(Mapper.map(movimiento, MovimientoEntity.class))))
                .flatMap(v -> Uni.createFrom().voidItem());
    }

    public Multi<Movimiento> listarMovimientos() {
        return sessionFactory.withSession(session -> session.createNamedQuery("MovimientoEntity.findAll", MovimientoEntity.class)
                        .getResultList()).map(u -> u.stream().map(v -> Mapper.map(v, Movimiento.class)).collect(Collectors.toList()))
                .onItem().transformToMulti(c -> Multi.createFrom().iterable(c));
    }

    public Multi<Movimiento> listarMovimientosPorFechas(Integer idCuenta, LocalDateTime fechaInicio, LocalDateTime fechaFin) {
        return sessionFactory.withSession(session -> session.createNamedQuery("MovimientoEntity.findByFecha", MovimientoEntity.class)
                        .setParameter("id", idCuenta)
                        .setParameter("fechaInicio", fechaInicio)
                        .setParameter("fechaFin", fechaFin)
                        .getResultList()).map(u -> u.stream().map(v -> Mapper.map(v, Movimiento.class)).collect(Collectors.toList()))
                .onItem().transformToMulti(c -> Multi.createFrom().iterable(c));
    }



}
