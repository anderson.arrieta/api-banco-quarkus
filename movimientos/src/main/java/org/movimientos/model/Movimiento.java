package org.movimientos.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Movimiento {
    private Integer id;
    private Integer idCuenta;
    private LocalDateTime fechaMovimiento;
    private Float saldoInicial;
    private String tipoMovimiento;
    private Float valor;
    private Float saldo;
}
