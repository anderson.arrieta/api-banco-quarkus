package org.movimientos.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Cuenta {
    private Integer id;
    private Integer idCliente;
    private String numeroCuenta;
    private String tipoCuenta;
    private Float saldoInicial;
    private String estado;
}
