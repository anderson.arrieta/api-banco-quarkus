package org.movimientos.clients;

import io.smallrye.mutiny.Uni;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.movimientos.model.Cliente;

@Path("/clientes")
@RegisterRestClient(baseUri = "http://localhost:8080")
public interface ClienteClient {

    @GET
    @Path("/{id}")
    Uni<Cliente> buscarCliente(@PathParam("id") Integer id);

}
