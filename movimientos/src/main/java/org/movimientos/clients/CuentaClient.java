package org.movimientos.clients;

import io.smallrye.mutiny.Uni;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.movimientos.dto.SaldoRequest;
import org.movimientos.model.Cuenta;

@Path("/cuentas")
@RegisterRestClient(baseUri = "http://localhost:8081")
public interface CuentaClient {

    @GET
    @Path("/{id}")
    Uni<Cuenta> buscarCuenta(@PathParam("id") Integer id);

    @PUT
    @Path("/{id}")
    Uni<Void> actualizarSaldo(@PathParam("id") Integer id, SaldoRequest request);

}
