package org.movimientos.service;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.movimientos.clients.ClienteClient;
import org.movimientos.clients.CuentaClient;
import org.movimientos.dto.MovimientoRequest;
import org.movimientos.dto.MovimientoResponse;
import org.movimientos.dto.SaldoRequest;
import org.movimientos.model.Movimiento;
import org.movimientos.repository.MovimientoRepository;

import java.time.LocalDateTime;

@ApplicationScoped
public class MovimientoService {

    @Inject
    @RestClient
    ClienteClient clienteClient;

    @Inject
    @RestClient
    CuentaClient cuentaClient;

    @Inject
    MovimientoRepository repository;

    public Uni<MovimientoResponse> realizarMovimiento(MovimientoRequest request) {

        return cuentaClient.buscarCuenta(request.getIdCuenta())
                .onItem().transformToUni(cuenta -> clienteClient.buscarCliente(cuenta.getIdCliente())
                        .onItem().transformToUni(cliente -> {

                            Float saldoInicial = cuenta.getSaldoInicial();
                            Float saldoDisponible = 0.0F;

                            if(request.getTipoMovimiento().equals("DEPOSITO")) {
                                saldoDisponible = cuenta.getSaldoInicial() + request.getValor();
                            } else if (request.getTipoMovimiento().equals("RETIRO")) {
                                if(saldoInicial >= request.getValor()) {
                                    saldoDisponible = saldoInicial - request.getValor();
                                } else {
                                    saldoDisponible = saldoInicial;
                                    return Uni.createFrom().failure(new Exception("fondos insuficientes."));
                                }
                            } else {
                                saldoDisponible = saldoInicial;
                                return Uni.createFrom().failure(new Exception("Error ingresando datos de transaccion."));
                            }

                            Float finalSaldoDisponible = saldoDisponible;

                            return cuentaClient.actualizarSaldo(cuenta.getId(), SaldoRequest.builder().saldo(saldoDisponible).build())
                                    .onItem().transformToUni(ignore -> repository.guardarMovimiento(Movimiento.builder()
                                            .idCuenta(cuenta.getId())
                                            .fechaMovimiento(LocalDateTime.now())
                                            .saldoInicial(saldoInicial)
                                            .tipoMovimiento(request.getTipoMovimiento())
                                            .valor(request.getValor())
                                            .saldo(finalSaldoDisponible)
                                            .build()))
                                    .map(movimiento -> MovimientoResponse.builder()
                                            .cliente(cliente.getNombre())
                                            .fechaMovimiento(LocalDateTime.now())
                                            .numeroCuenta(cuenta.getNumeroCuenta())
                                            .tipoCuenta(cuenta.getTipoCuenta())
                                            .saldoInicial(saldoInicial)
                                            .tipoMovimiento(request.getTipoMovimiento())
                                            .valorMovimiento(request.getValor())
                                            .saldoDisponible(finalSaldoDisponible)
                                            .build());
                        }));

    }

    public Multi<MovimientoResponse> listarMovimientos() {
        return repository.listarMovimientos()
                .flatMap(movimiento -> cuentaClient.buscarCuenta(movimiento.getIdCuenta())
                        .onItem().transformToUni(cuenta -> clienteClient.buscarCliente(cuenta.getIdCliente())
                                .onItem().transform(cliente -> MovimientoResponse.builder()
                                        .fechaMovimiento(movimiento.getFechaMovimiento())
                                        .cliente(cliente.getNombre())
                                        .numeroCuenta(cuenta.getNumeroCuenta())
                                        .tipoCuenta(cuenta.getTipoCuenta())
                                        .saldoInicial(movimiento.getSaldoInicial())
                                        .tipoMovimiento(movimiento.getTipoMovimiento())
                                        .valorMovimiento(movimiento.getValor())
                                        .saldoDisponible(movimiento.getSaldo())
                                        .build())).toMulti());
    }

    public Multi<MovimientoResponse> listarMovimientosPorFechas(Integer idCuenta, LocalDateTime fechaInicio, LocalDateTime fechaFin) {
        return repository.listarMovimientosPorFechas(idCuenta, fechaInicio, fechaFin)
                .flatMap(movimiento -> cuentaClient.buscarCuenta(movimiento.getIdCuenta())
                        .onItem().transformToUni(cuenta -> clienteClient.buscarCliente(cuenta.getIdCliente())
                                .onItem().transform(cliente -> MovimientoResponse.builder()
                                        .fechaMovimiento(movimiento.getFechaMovimiento())
                                        .cliente(cliente.getNombre())
                                        .numeroCuenta(cuenta.getNumeroCuenta())
                                        .tipoCuenta(cuenta.getTipoCuenta())
                                        .saldoInicial(movimiento.getSaldoInicial())
                                        .tipoMovimiento(movimiento.getTipoMovimiento())
                                        .valorMovimiento(movimiento.getValor())
                                        .saldoDisponible(movimiento.getSaldo())
                                        .build())).toMulti());
    }

}
