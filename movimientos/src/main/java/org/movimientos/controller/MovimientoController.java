package org.movimientos.controller;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.movimientos.dto.MovimientoRequest;
import org.movimientos.dto.MovimientoResponse;
import org.movimientos.dto.RangoFechasRequest;
import org.movimientos.model.Movimiento;
import org.movimientos.service.MovimientoService;

@Path("/movimientos")
public class MovimientoController {

    @Inject
    MovimientoService service;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Response> realizarMovimiento(MovimientoRequest request) {
        return service.realizarMovimiento(request)
                .map(response -> Response.status(Response.Status.OK).entity(response).build())
                .onFailure().invoke(throwable -> Response.serverError().build());
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Multi<Response> listarMovimientos() {
        return service.listarMovimientos()
                .map(response -> Response.status(Response.Status.OK).entity(response).build())
                .onFailure().invoke(throwable -> Response.serverError().build());
    }

    @POST
    @Path("/fechas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Multi<Response> listarMovimientosPorFechas(RangoFechasRequest request) {
        return service.listarMovimientosPorFechas(request.getIdCuenta(), request.getFechaInicio(), request.getFechaFin())
                .map(response -> Response.status(Response.Status.OK).entity(response).build())
                .onFailure().invoke(throwable -> Response.serverError().build());
    }

}
