package org.movimientos.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MovimientoResponse {
    private LocalDateTime fechaMovimiento;
    private String cliente;
    private String numeroCuenta;
    private String tipoCuenta;
    private Float saldoInicial;
    private String tipoMovimiento;
    private Float valorMovimiento;
    private Float saldoDisponible;

}
