package org.movimientos.controller;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;
import org.movimientos.dto.MovimientoRequest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
class MovimientosTest {

    @Test
    void realizarMovimiento() {
        MovimientoRequest request = MovimientoRequest.builder()
                .idCuenta(1)
                .valor(3200.00F)
                .tipoMovimiento("RETIRO")
                .build();

        given()
                .when()
                .contentType(ContentType.JSON)
                .body(request)
                .post()
                .then()
                .statusCode(200);


    }

    @Test
    void listarMovimientosPorFechas() {

    }
}