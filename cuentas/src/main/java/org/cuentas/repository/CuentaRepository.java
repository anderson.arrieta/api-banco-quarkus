package org.cuentas.repository;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManagerFactory;
import org.cuentas.mapper.Mapper;
import org.cuentas.model.Cuenta;
import org.cuentas.repository.entities.CuentaEntity;
import org.hibernate.reactive.mutiny.Mutiny;

import java.util.stream.Collectors;

@ApplicationScoped
public class CuentaRepository {

    private final Mutiny.SessionFactory sessionFactory;

    public CuentaRepository(EntityManagerFactory entityManagerFactory) {
        this.sessionFactory = entityManagerFactory.unwrap(Mutiny.SessionFactory.class);
    }

    public Uni<Void> crearCuenta(Cuenta cuenta) {
        return sessionFactory.withTransaction(((session, transaction) -> session.persist(Mapper.map(cuenta, CuentaEntity.class))))
                .flatMap(v -> Uni.createFrom().voidItem());
    }

    public Multi<Cuenta> listarCuentas() {
        return sessionFactory.withSession(session -> session.createNamedQuery("CuentaEntity.findAll", CuentaEntity.class)
                        .getResultList()).map(u -> u.stream().map(v -> Mapper.map(v, Cuenta.class)).collect(Collectors.toList()))
                .onItem().transformToMulti(c -> Multi.createFrom().iterable(c));
    }

    public Uni<Cuenta> buscarCuenta(Integer id) {
        return sessionFactory.withSession(session -> session.find(CuentaEntity.class, id))
                .onItem().transform(c -> Mapper.map(c, Cuenta.class));
    }

    public Uni<Void> actualizarSaldo(Integer id, Float saldo) {
        return sessionFactory.withTransaction((session, transaction) -> {
            return session.find(CuentaEntity.class, id)
                    .onItem().ifNotNull().transform(cuenta -> {
                        cuenta.setSaldoInicial(saldo);
                        return cuenta;
                    }).onItem().ignore().andContinueWithNull();
        });
    }

}
