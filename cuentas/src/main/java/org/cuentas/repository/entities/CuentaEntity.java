package org.cuentas.repository.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "cuentas")
@NamedQuery(name = "CuentaEntity.findAll", query = "SELECT c FROM CuentaEntity c")
public class CuentaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer idCliente;
    private String numeroCuenta;
    private String tipoCuenta;
    private Float saldoInicial;
    private String estado;

}
