package org.cuentas.service;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.cuentas.model.Cuenta;
import org.cuentas.repository.CuentaRepository;

@ApplicationScoped
public class CuentaService {

    @Inject
    CuentaRepository repository;

    public Uni<Void> crearCuenta(Cuenta cuenta) {
        return repository.crearCuenta(cuenta);
    }

    public Multi<Cuenta> listarCuentas() {
        return repository.listarCuentas();
    }

    public Uni<Cuenta> buscarCuenta(Integer id) {
        return repository.buscarCuenta(id);
    }

    public Uni<Void> actualizarSaldo(Integer id, Float saldo) {
        return repository.actualizarSaldo(id, saldo);
    }

}
