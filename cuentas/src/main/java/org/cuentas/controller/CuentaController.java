package org.cuentas.controller;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.cuentas.dtos.SaldoRequest;
import org.cuentas.model.Cuenta;
import org.cuentas.service.CuentaService;

@Path("/cuentas")
public class CuentaController {

    @Inject
    CuentaService service;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Void> crearCuenta(Cuenta cuenta) {
        return service.crearCuenta(cuenta);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Multi<Cuenta> listarCuentas() {
        return service.listarCuentas();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Cuenta> buscarCuenta(@PathParam("id") Integer id) {
        return service.buscarCuenta(id);
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Void> actualizarSaldo(@PathParam("id") Integer id, SaldoRequest request) {
        return service.actualizarSaldo(id, request.getSaldo());
    }

}
